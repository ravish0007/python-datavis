## [Data Visualization with Python]()

<br>

### AMC
#### November 10-11 2023

-----

# Introduction

---

## Session

- Hands On <!-- .element: class="fragment " data-fragment-index="0" -->
- Data Visualization Basics <!-- .element: class="fragment " data-fragment-index="1" -->
- [BCS358D Lab](img/BCS358D.pdf) <!-- .element: class="fragment " data-fragment-index="2" -->
- Requires basic python <!-- .element: class="fragment " data-fragment-index="3" -->

---

## Follow Along

<a href="https://live-presentation.fsmk.org">
live-presentation.fsmk.org
</a>

-----

# Installation

1. Connect to network

#### SSID : <SSID>
#### PASS : <PASS> 

2. Click [here]() for the materials.

---

## Verify Installation

### Linux :
Run 'conda' on to verify Anacondas installation.

-----

# [Data Science]()

---

## Data ?

---

Data - lat. plural for <!-- .element: class="fragment" data-fragment-index="0" --> <em>Datum</em> <!-- .element: class="fragment" data-fragment-index="1" --> 

i.e a fact or proposition <!-- .element: class="fragment" data-fragment-index="2" -->


Data = Collection of facts. <!-- .element: class="fragment" data-fragment-index="3" -->

---

## Science ?

---

<em>Techniques and frameworks to understand topics using experimental observations and mathematical inference.</em>

---

<div class="r-stack">

![](img/datascience1.png) <!-- .element: class="fragment current-visible" data-fragment-index="0" -->

![](img/datascience2.png) <!-- .element: class="fragment current-visible" data-fragment-index="1" -->

![](img/datascience3.png) <!-- .element: class="fragment current-visible" data-fragment-index="2" -->

</div>

---

If we define science [ broadly ] as

<br>

<em>"Applications of math pertaining to the world"</em>

---

Then, Data Science :

<em>"Applied statistics, informed by the study of algorithms and informatics, to process data in computerized environments"</em>

-----

# [Data Science Lifecycle]()

---

- Data Ingestion <!-- .element: class="fragment" data-fragment-index="0" --> 
- Data Storage <!-- .element: class="fragment" data-fragment-index="1" -->
- Data Processing <!-- .element: class="fragment" data-fragment-index="2" -->
- Data Analysis <!-- .element: class="fragment" data-fragment-index="3" -->
- Data Visualization <!-- .element: class="fragment" data-fragment-index="4" --> <!-- .element: style="color:#fa0" data-fragment-index="5" -->

-----

# [Data Visualization]()

---

## Why [Datavis]() ?

- Data is abstract <!-- .element: class="fragment" data-fragment-index="0" -->
- Visual Storytelling <!-- .element: class="fragment" data-fragment-index="1" -->
- Easier to Infer Trends <!-- .element: class="fragment" data-fragment-index="2" -->
- Helps Decision Making <!-- .element: class="fragment" data-fragment-index="3" -->

---

## [Tools]()

---

Data visualization requires 3 ingredients :

- Data <!-- .element: class="fragment" data-fragment-index="0" -->
- Data processing framework/platform <!-- .element: class="fragment" data-fragment-index="1" -->
- Visualisation tools <!-- .element: class="fragment" data-fragment-index="2" --> 

---

## Data

[ Link to Data !! &&& ]()

---

## Data Processing Platforms

Tools and languages that can work together to process data.

[Examples]() : <!-- .element: class="fragment" data-fragment-index="0" -->

- Python - NumPy, Pandas <!-- .element: class="fragment" data-fragment-index="1" -->
- Tableu <!-- .element: class="fragment" data-fragment-index="2" -->
- R - RStudio <!-- .element: class="fragment" data-fragment-index="3" -->
- Julia <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Data Visualisation Tools

Frameworks, libraries and services that integrate with Data Processing Platforms/

[Examples]() : <!-- .element: class="fragment" data-fragment-index="0" -->

- Python - Matplotlib, Seaborn, Bokeh, Plotly, Altair, Holoviz, Vizpy, Napari <!-- .element: class="fragment" data-fragment-index="1" -->
- JS - p5.js, d3.js, Observable <!-- .element: class="fragment" data-fragment-index="3" -->

-----

## DataVis : How To Choose a Chart

[Resource for Chart Decision](https://extremepresentation.typepad.com/files/choosing-a-good-chart-09.pdf)

-----

## Setting up Anaconda

Python Runtime

- [Website]()
- [Documentation]()

-----

## NumPy

Python Framework for Numerical Computing

- High Performance
- N-Dimensional Arrays
- Interoperable

---

## NumPy

- [Website]()
- [Documentation]()

---

## Where is NumPy used ?

- BioInformatics
- Quantum Computing
- Signal + Image Processing
- AI/ML
- Astronomy & Cosmology
- Networking

---

## NumPy exercises

[Notebook]()


-----

## Pandas

---

## Pandas excercises

[Notebook]()

-----

## Matplotlib



-----

## Seaborn

-----

## Bokeh

-----

## Plotly

-----

## Libreoffice

-----

Thank you !

-----

Feedback

